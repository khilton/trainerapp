import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import { AppLoading, Asset, Font, Icon } from 'expo';
import firebase from '@firebase/app';
import '@firebase/auth';
import AppNavigator from './navigation/AppNavigator';
import combineReducers from './src/reducers';

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  componentWillMount(){
      const config = {
        apiKey: "AIzaSyCoAvTlzA2in5-4pP-30hKQfI9kIXBpNR4",
        authDomain: "dev-player-77051.firebaseapp.com",
        databaseURL: "https://dev-player-77051.firebaseio.com",
        projectId: "dev-player-77051",
        storageBucket: "dev-player-77051.appspot.com",
        messagingSenderId: "929524094067"
    };
    firebase.initializeApp(config);
  }

  render() {
    const store = createStore( combineReducers, {}, applyMiddleware(ReduxThunk));

   if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <Provider store={store}>
          <View style={styles.container}>
            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
            <AppNavigator />
          </View>
        </Provider>
      );
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/robot-dev.png'),
        require('./assets/images/robot-prod.png'),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      }),
    ]);
  };


  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
