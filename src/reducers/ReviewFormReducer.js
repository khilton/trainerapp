import {
  REVIEW_UPDATE,
  REVIEW_CREATE,
  REVIEWS_SAVE_SUCCESS,
} from '../actions/reviewTypes';

const INITIAL_STATE = {
  sessionType: '',
  date: new Date().toString(),
  positive1: '',
  positive2: '',
  positive3: '',
  improvement1: '',
  improvement2: '',
  improvement3: '',
  enjoyable: '',
  effort: '',
  focus: '',
};

export default (state = INITIAL_STATE, action) => {

  switch (action.type) {
    case REVIEW_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    case REVIEW_CREATE:
      return INITIAL_STATE;
    case REVIEWS_SAVE_SUCCESS:
      return INITIAL_STATE;
    default:
      return state;
  }
};
