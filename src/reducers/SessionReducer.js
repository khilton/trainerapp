import {
  CHANGE_DRILL_INDEX,
  SET_CURRENT_DRILL,
  GET_DRILLS,
  GET_SESSIONS,
  GET_DRILLS_SUCCESS,
  GET_DRILLS_FAIL,
  LOAD_SESSION,
  LOAD_SESSION_SUCCESS,
  LOAD_SESSION_FAILURE,
  PLAY_SESSION,
  PAUSE_SESSION
 } from '../actions/sessionTypes';
//a session is essentially a playlist of drills

const INITIAL_STATE= {
  drillIndex: 0,
  currentDrill: {},
  drills: [],
  sessions: [],
  sessionLoading: true,
  sessionError: '',
  playMainVideo: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CHANGE_DRILL_INDEX:
      return { ...state, drillIndex: action.payload };
    case SET_CURRENT_DRILL:
      return { ...state, currentDrill: action.payload };
    case GET_DRILLS:
      return { ...state, sessionError: '',};
    case GET_DRILLS_SUCCESS:
      return { ...state, drills: action.payload};
    case GET_DRILLS_FAIL:
      return { ...state, sessionError: 'Unable to load drills.'};
    case GET_SESSIONS:
      return { ...state, sessions: action.payload};
    case LOAD_SESSION:
      return { ...state, sessionLoading: true};
    case LOAD_SESSION_SUCCESS:
      return { ...state, sessionLoading: false};
    case LOAD_SESSION_FAILURE:
      return { ...state, sessionLoading: false};
    case PLAY_SESSION:
      return { ...state, playMainVideo: true};
    case PAUSE_SESSION:
      return { ...state, playMainVideo: false};
    default:
      return state;
  }
};
