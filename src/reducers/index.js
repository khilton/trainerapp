import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import ReviewFormReducer from './ReviewFormReducer';
import ReviewsReducer from './ReviewsReducer';
import SessionReducer from './SessionReducer';

export default combineReducers({
  auth: AuthReducer,
  reviewForm: ReviewFormReducer,
  reviews: ReviewsReducer,
  sessionReducer: SessionReducer,
});
