import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  REGISTER_USER_FAIL,
  REGISTER_USER,
  PROFILE_UPDATE
} from '../actions/authTypes';

const INITIAL_STATE = {
  email: '',
  password: '',
  user: null,
  error: '',
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EMAIL_CHANGED:
      return { ...state, email: action.payload };
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload };
    case LOGIN_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, user: action.payload, loading: false };
    case LOGIN_USER_FAIL:
        return { ...state, error: 'Authentication Failed. Email or password is incorrect.', loading: false };
    case LOGIN_USER:
        return { ...state, loading: true, error: '' };
    case REGISTER_USER_FAIL:
        return { ...state, error: 'Sign up failed. Email is already in use.', loading: false };
    case REGISTER_USER:
        return { ...state, loading: true, error: '' };
    case PROFILE_UPDATE:
      return { ...state, [action.payload.prop]: action.payload.value };
    default:
      return state;
  }
};
