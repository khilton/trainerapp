import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, Dimensions } from 'react-native';
import { phoneChanged, registerUser, loginUser } from '../../actions';
import {
  Header,
  AuthSpinner,
  AuthCard,
  AuthCardSection,
  AuthInput,
  AuthButton
} from '../common';

class SignUpForm extends Component {
  onPhoneChange(text) {
    this.props.phoneChanged(text);
  }

  renderNextButton() {
    if (this.props.loading) {
      return <AuthSpinner size="large" />;
    }

    return (
      <AuthButton onPress={this.onNextButtonPress.bind(this)}>
        Next
      </AuthButton>
    );
  }

  renderLoginButton() {
    return (
      <AuthButton onPress={this.onLoginButtonPress.bind(this)}>
      Login, here.
      </AuthButton>
    );
  }

  onSignUpButtonPress(text) {
    //this.props.navigation.navigate({
        //routeName: 'signUp'
    //});
    //const { email, password, error, navigation } = this.props;
    this.props.registerUser({ phone, error, navigation });
  }

  onLoginButtonPress() {
      this.props.navigation.navigate({
          routeName: 'login'
      });
  }

  render() {
    return(
      <View style={styles.mainContainerStyle}>
        <Image
          source={require('../resources/11devHomescreen.png')}
          style={{width: Dimensions.get('window').width, height: 300, alignSelf: 'center'}}
        />
        <View>
          <AuthCard>
            <AuthCardSection>
              <AuthInput
                label="Phone"
                placeHolder="555-555-5555"
                onChangeText={this.onPhoneChange.bind(this)}
                value={this.props.phone}
                />
            </AuthCardSection>
          </AuthCard>

          <View style={styles.containerStyle}>
            <Text style={styles.errorTextStyle}>
              {this.props.error}
            </Text>
          </View>

          <View style={styles.containerStyle}>
            {this.renderNextButton()}
          </View>

        </View>

        <View>
          <View style={styles.textContainerStyle}>
            <Text style={styles.questionTextStyle}>Already registered?</Text>
          </View>
          <View style={styles.containerStyle}>
            {this.renderLoginButton()}
          </View>
        </View>
      </View>

    );
  }
}

const styles = {
  mainContainerStyle: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: '#008901',
  },
  errorTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    color: '#c9e265',
  },
  questionTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    justifyContent: 'center',
    color: '#fffceb',
  },
  containerStyle: {
    padding: 5,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
  },
  textContainerStyle: {
    padding: 5,
  }
}

const mapStateToProps = ({ auth }) => {
  const { phone, error, loading } = auth;

  return {
    phone: phone,
    error: error,
    loading: loading,
  };
};

//Change action from login to sign up
export default connect( mapStateToProps,
  {  phoneChanged,  registerUser })(SignUpForm);
