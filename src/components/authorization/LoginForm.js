import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, Dimensions } from 'react-native';
import { phoneChanged, codeChanged, loginUser } from '../../actions';
import {
  Header,
  AuthSpinner,
  AuthCard,
  AuthCardSection,
  AuthInput,
  AuthButton
} from '../common';

class LoginForm extends Component {
  onPhoneChange(text) {
    this.props.phoneChanged(text);
  }

  onCodeChange(text) {
    this.props.codeChanged(text);
  }

  renderLoginButton() {
    if (this.props.loading) {
      return <AuthSpinner size="large" />;
    }

    return (
      <AuthButton onPress={this.onLoginButtonPress.bind(this)}>
      Login
      </AuthButton>
    );
  }

  renderSignUpButton() {
    return (
      <AuthButton onPress={this.onSignUpButtonPress.bind(this)}>
      Don't have a code yet? Get one, here.
      </AuthButton>
    );
  }

  onLoginButtonPress(text) {
    const { phone, code, error, navigation } = this.props;
    this.props.loginUser({ phone, code, error, navigation });
  }

  onSignUpButtonPress() {
      this.props.navigation.navigate('signUp');
  }

  render() {
    return(
        <View style={styles.mainContainerStyle}>
          <Image
            source={require('../resources/11devHomescreen.png')}
            style={{width: Dimensions.get('window').width, height: 300, alignSelf: 'center'}}
          />
          <View>
            <AuthCard style={{ flex: 1 }}>
              <AuthCardSection>
                <AuthInput
                  label="Phone"
                  placeHolder="555-555-5555"
                  onChangeText={this.onPhoneChange.bind(this)}
                  value={this.props.phone}
                  />
              </AuthCardSection>

              <AuthCardSection>
                <AuthInput
                  label="Code"
                  placeholder="code"
                  onChangeText={this.onCodeChange.bind(this)}
                  value={this.props.code}
                  secureTextEntry
                  />
              </AuthCardSection>
            </AuthCard>

            <View style={styles.containerStyle}>
              <Text style={styles.errorTextStyle}>
                {this.props.error}
              </Text>
            </View>

            <View style={styles.containerStyle}>
              {this.renderLoginButton()}
            </View>
          </View>

          <View>
            <View style={styles.textContainerStyle}>
              <Text style={styles.questionTextStyle}>Not registered yet?</Text>
            </View>
            <View style={styles.containerStyle}>
              {this.renderSignUpButton()}
            </View>
          </View>
      </View>

    );
  }
}

const styles = {
  mainContainerStyle: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: '#008901',
  },
  errorTextStyle: {
    fontSize: 18,
    textAlign: 'center',
    color: '#c9e265',
  },
  questionTextStyle: {
    fontSize: 18,
    textAlign: 'center',
    color: '#fffceb',
  },
  containerStyle: {
    padding: 5,
    flexDirection: 'row',
  },
  textContainerStyle: {
    padding: 5,
  }
}

const mapStateToProps = ({ auth }) => {
  const { phone, code, error, loading } = auth;

  return {
    phone: phone,
    code: code,
    error: error,
    loading: loading,
  };
};

export default connect( mapStateToProps,
  { phoneChanged, codeChanged, loginUser })(LoginForm);
