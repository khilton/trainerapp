import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, Image, Dimensions, Picker } from 'react-native';
import { emailChanged, passwordChanged, registerUser, loginUser, profileUpdate } from '../../actions';
import {
  Header,
  AuthSpinner,
  AuthCard,
  AuthCardSection,
  AuthInput,
  AuthButton
} from '../common';

class SignUpForm extends Component {
  onComponentWillMount(){
    console.log(this.props);
  }
  onProfileChange(text) {
    this.props.profileUpdate(text);
  }

  renderSignUpButton() {
    if (this.props.loading) {
      return <AuthSpinner size="large" />;
    }

    return (
      <AuthButton onPress={this.onSignUpButtonPress.bind(this)}>
      Sign Up
      </AuthButton>
    );
  }

  renderLoginButton() {
    return (
      <AuthButton onPress={this.onLoginButtonPress.bind(this)}>
      Login, here.
      </AuthButton>
    );
  }

  onSignUpButtonPress(text) {
    const { email, password, error, navigation } = this.props;
    this.props.registerUser({ email, password, error, navigation });
  }

  onLoginButtonPress() {
      this.props.navigation.navigate({
          routeName: 'login'
      });
  }

  render() {
    return(
      <View style={styles.mainContainerStyle}>
        <View>
          <Text>Before we get you signed up, tell us a bit more about yourself.</Text>

          <AuthCard>
            <AuthCardSection>
              <AuthInput
                label="Name"
                onChangeText={this.onProfileChange.bind(this)}
                value={this.props.name}
                />
            </AuthCardSection>

            <AuthCardSection>
              <AuthInput
                label="City"
                onChangeText={this.onProfileChange.bind(this)}
                value={this.props.city}
                />
            </AuthCardSection>

            <AuthCardSection>
              <AuthInput
                label="Province/State"
                onChangeText={this.onProfileChange.bind(this)}
                value={this.props.region}
                />
            </AuthCardSection>

            <AuthCardSection>
              <Picker
                  selectedValue={this.props.ageGroup}
                  style={styles.pickerStyle}
                  onValueChange={value => this.props.profileUpdate({prop: 'ageGroup', value})}>
                  <Picker.Item label="U12" value="U12" />
                  <Picker.Item label="U13" value="U13" />
                  <Picker.Item label="U14" value="U14" />
                  <Picker.Item label="U15" value="U15" />
                  <Picker.Item label="U16" value="U16" />
                  <Picker.Item label="U17" value="U17" />
                  <Picker.Item label="U18" value="U18" />
              </Picker>
            </AuthCardSection>

            <AuthCardSection>
              <Picker
                  selectedValue={this.props.tier}
                  style={styles.pickerStyle}
                  onValueChange={value => this.props.profileUpdate({prop: 'tier', value})}>
                  <Picker.Item label="1" value="1" />
                  <Picker.Item label="2" value="2" />
                  <Picker.Item label="3" value="3" />
                  <Picker.Item label="4" value="4" />
                  <Picker.Item label="5" value="5" />
              </Picker>
            </AuthCardSection>



          </AuthCard>

          <View style={styles.containerStyle}>
            <Text style={styles.errorTextStyle}>
              {this.props.error}
            </Text>
          </View>

          <View style={styles.containerStyle}>
            {this.renderSignUpButton()}
          </View>

        </View>

        <View>
          <View style={styles.textContainerStyle}>
            <Text style={styles.questionTextStyle}>Already registered?</Text>
          </View>
          <View style={styles.containerStyle}>
            {this.renderLoginButton()}
          </View>
        </View>
      </View>

    );
  }
}

const styles = {
  mainContainerStyle: {
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: '#008901',
  },
  errorTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    color: '#c9e265',
  },
  questionTextStyle: {
    fontSize: 18,
    alignSelf: 'center',
    justifyContent: 'center',
    color: '#fffceb',
  },
  containerStyle: {
    padding: 5,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
  },
  textContainerStyle: {
    padding: 5,
  },
  pickerStyle: {
    flex: 1,
  },
  pickerLabelStyle: {
    fontSize: 18,
    paddingLeft:20,
  }
}

const mapStateToProps = ({ auth }) => {
  const { email, password, error, loading, name, city } = auth;

  return {
    email: email,
    password: password,
    error: error,
    loading: loading,
    name: name,
    city: city
  };
};

//Change action from login to sign up
export default connect( mapStateToProps,
  {  emailChanged, passwordChanged, registerUser, profileUpdate })(SignUpForm);
