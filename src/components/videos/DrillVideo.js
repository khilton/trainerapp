import React, { Component } from 'react';
import { View, Dimensions } from 'react-native';
import { Video } from 'expo';

class DrillVideo extends Component {

  render(){
    const {height, width} = Dimensions.get('window');
    return(
      <View style={{flex:1}}>
        <Video
          source= {{uri: this.props.videoURI}}
          rate={1.0}
          volume={1.0}
          isMuted={false}
          resizeMode="cover"
          shouldPlay={this.props.shouldPlay}
          useNativeControls={false}
          isLooping
          style={{ width: width, height: 220 }}
        />
      </View>
    );
  }
};

export default DrillVideo;
