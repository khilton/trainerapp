import _ from 'lodash';
import React, { Component } from 'react';
import { View, ListView, Text } from 'react-native';
import { Card } from '../common';
import DrillListItem from './DrillListItem';

class DrillList extends Component {

  componentWillMount(){
    this.createDataSource(this.props.drills);
  }

  componentWillReceiveProps(nextProps) {
    // nextProps are the next set of nextProps
    //this.props is still the old set
    this.createDataSource(nextProps.drills);
  }

  createDataSource(drills) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataSource = ds.cloneWithRows(drills);
  }

  renderRow(drill){
    return (
      <DrillListItem
        drill={drill}
      />
    );
  }

  render(){
    return(
      <Card>
        <ListView
          enableEmptySections
          dataSource={this.dataSource}
          renderRow={this.renderRow}
        />
      </Card>
    );
  }
};


export default DrillList;
