import React, { Component } from 'react';
import {
    ScrollView,
    Text,
    View
} from 'react-native';
import { connect } from 'react-redux';
import { Button, Card, CardSection, Spinner } from '../common';
import { changeDrillIndex, setCurrentDrill, getDrills, loadSession, playSession, pauseSession } from '../../actions';
import DrillVideo from './DrillVideo';
import DrillList from './DrillList';

class SessionScreen extends Component {


  componentWillMount(){
    var session = this.props.navigation.state.params.session;
    this.props.loadSession(session.drills);
  }

  setDrill(drillIndex, drills) {
    this.props.changeDrillIndex(drillIndex)
    this.props.setCurrentDrill(drillIndex, drills);
  }

  onNextButtonPress() {
    this.setDrill(this.props.drillIndex + 1, this.props.drills)
  }

  onPausePress() {
    this.props.pauseSession();
  }

  onPlayPress() {
    this.props.playSession();
  }

  renderPausePlayButton() {
    if ( this.props.playMainVideo ) {
      return(<Button onPress={this.onPausePress.bind(this)}>Pause</Button>);
    } else {
      return(<Button onPress={this.onPlayPress.bind(this)}>Play</Button>);
    };
  }

  renderSessionScreen(){
    return (
      <ScrollView style = {{flex: 1}}>
          <DrillVideo
            videoURI = {this.props.currentDrill.videoURI}
            shouldPlay = {this.props.playMainVideo}
          />
        <Card>
          <CardSection>
            <Text style={styles.nameTextStyle}>{this.props.currentDrill.name}</Text>
          </CardSection>
          <CardSection>
            <Text style={styles.durationTextStyle}>Instructions: {this.props.currentDrill.instructions}</Text>
          </CardSection>
          <CardSection>
            <Button onPress={this.onNextButtonPress.bind(this)}>Next</Button>
          </CardSection>
          <CardSection>
            {this.renderPausePlayButton()}
          </CardSection>
        </Card>
        <DrillList
          drills={this.props.drills}
        />
      </ScrollView>
    );
  }

  renderSpinner(){
    return(
      <View style={{flex: 1}}>
        <Spinner size="large"/>
      </View>
    );
  }

  render() {
    if ( this.props.sessionLoading ) {
      return(this.renderSpinner());
    } else {
      return(this.renderSessionScreen());
    };
  };
}

const styles = {
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  nameTextStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: 'left',
    color: '#000000',
    textTransform: 'uppercase',
    paddingLeft: 10,
    paddingTop: 5,
    paddingBottom:5,
  },
  durationTextStyle: {
    flex: 1,
    fontSize: 12,
    textAlign: 'left',
    color: '#000000',
    paddingLeft: 10,
    paddingBottom: 5,
    paddingTop:5
  },
};

const mapStateToProps = ({ sessionReducer }) => {
  const { drillIndex, currentDrill, drills, sessionLoading, playMainVideo } = sessionReducer;

  return {
    drillIndex: drillIndex,
    currentDrill: currentDrill,
    drills: drills,
    sessionLoading: sessionLoading,
    playMainVideo: playMainVideo
  };
};

export default connect( mapStateToProps,
  { changeDrillIndex, setCurrentDrill, getDrills, loadSession, playSession, pauseSession })(SessionScreen);
