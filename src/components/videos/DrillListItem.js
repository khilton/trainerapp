import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { CardSection } from '../common';
import DrillThumbnailVideo from './DrillThumbnailVideo';
import { changeDrillIndex, setCurrentDrill } from '../../actions';

//this could be a stateless component
class DrillListItem extends Component {

  onRowPress() {
      this.props.changeDrillIndex(this.props.drill.sessionIndex);
      this.props.setCurrentDrill(this.props.drill.sessionIndex, this.props.drills);
  }

  render(){
    return(
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View style={styles.mainContainerStyle}>
          <CardSection>
            <View>
              <View>
                <Text style={styles.nameTextStyle}>{this.props.drill.name}</Text>
              </View>
              <View>
                <Text style={styles.durationTextStyle}>{this.props.drill.duration}</Text>
              </View>
            </View>
          </CardSection>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  mainContainerStyle: {
    flex: 1,
    justifyContent: 'right',
  },
  nameTextStyle: {
    flex: 1,
    fontSize: 14,
    textAlign: 'left',
    color: '#000000',
    textTransform: 'uppercase',
    paddingLeft: 10,
    paddingTop: 5,
  },
  durationTextStyle: {
    flex: 1,
    fontSize: 10,
    textAlign: 'left',
    color: '#000000',
    paddingLeft: 10,
    paddingBottom: 5,
  },
}

const mapStateToProps = ({ sessionReducer }) => {
  const { drillIndex, drills } = sessionReducer;

  return {
    drillIndex: drillIndex,
    drills: drills
  };
};

export default connect( mapStateToProps,
  { changeDrillIndex, setCurrentDrill })(DrillListItem);
