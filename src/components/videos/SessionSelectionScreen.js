import React, { Component } from 'react';
import { ListView, View, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { getSessions } from '../../actions';
import { Card } from '../common';
import SessionListItem from './SessionListItem';

class SessionSelectionScreen extends Component {
  componentWillMount(){
    this.props.getSessions();
    this.createDataSource(this.props);
  }

  componentWillReceiveProps(nextProps) {
    // nextProps are the next set of nextProps
    //this.props is still the old set
    this.createDataSource(nextProps);
  }

  createDataSource({ sessions }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataSource = ds.cloneWithRows(sessions);
  }

  renderRow(session){
      return (
        <SessionListItem
          session={session}
        />
      );
  }

  render(){
    return(
      <View style={styles.mainContainerStyle}>
        <Image
          source={require('../resources/Images/TrainingSessions.png')}
          style={{width: Dimensions.get('window').width, height: 160, alignSelf: 'flex-end'}}
        />

        <ListView
          enableEmptySections
          dataSource={this.dataSource}
          renderRow={this.renderRow}
          style={{paddingTop:5}}
        />
      </View>
    );
  }
}


const styles = {
  mainContainerStyle: {
    flex: 1,
    justifyContent: 'right',
  },
}

const mapStateToProps = ({ sessionReducer }) => {
  const { sessions } = sessionReducer;

  return {
    sessions: sessions
  };
};

export default connect( mapStateToProps,
  { getSessions })(SessionSelectionScreen);
