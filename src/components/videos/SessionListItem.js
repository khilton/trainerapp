import React, { Component } from 'react';
import { View, Text, Image, TouchableWithoutFeedback } from 'react-native';
import { withNavigation } from 'react-navigation';
import { SessionCardSection } from '../common';
import { changeDrillIndex, setCurrentDrill } from '../../actions';

class SessionListItem extends Component {

  onRowPress() {
      this.props.navigation.navigate('sessionPage', { session: this.props.session });
  }

  render(){
    const imageURI = this.props.session.thumbnail;
    console.log(this.props.session)
    return(
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View style={styles.mainContainerStyle}>
          <SessionCardSection>
            <View>
              <Text style={styles.nameTextStyle}>{this.props.session.name}</Text>
              <Text style={styles.durationTextStyle}>Duration: {this.props.session.duration}</Text>
            </View>
            <Image
              style={styles.imageStyle}
              source={{uri: imageURI}}
            />
          </SessionCardSection>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  mainContainerStyle: {
    flex: 1,
    justifyContent: 'right',
  },
  nameTextStyle: {
    flex: 1,
    fontSize: 18,
    textAlign: 'left',
    color: '#000000',
    textTransform: 'uppercase',
    paddingLeft: 10,
    paddingTop: 25,
  },
  durationTextStyle: {
    flex: 1,
    fontSize: 12,
    textAlign: 'left',
    color: '#000000',
    paddingLeft: 10,
    paddingBottom: 5,
  },
  imageStyle: {
    width: 150,
    height: 80,
  }
}


export default withNavigation(SessionListItem);
