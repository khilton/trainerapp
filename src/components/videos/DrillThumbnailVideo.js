import React from 'react';
import { View } from 'react-native';
import { Video } from 'expo';

const DrillThumbnailVideo = ({ videoURI, shouldPlay }) => {
  return(
    <View  style={{felx: 1}}>
      <Video
        source= {{uri: videoURI}}
        rate={1.0}
        volume={1.0}
        isMuted={false}
        resizeMode="cover"
        shouldPlay={false}
        isLooping
        style={{ width: 40, height: 30}}
      />
    </View>
  );
};

export default DrillThumbnailVideo;
