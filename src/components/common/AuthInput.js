import React from 'react';
import { TextInput, View, Text } from 'react-native';

const AuthInput = ({
  label,
  value,
  onChangeText,
  placeHolder,
  secureTextEntry
}) => {

  const { inputStyle, labelStyle, containerStyle } = styles;

  return(
    <View style = {containerStyle}>
      <Text style = { labelStyle}> { label } </Text>
        <TextInput
          secureTextEntry = { secureTextEntry }
          placeholder={placeHolder}
          autoCorrect = { false }
          style = {inputStyle}
          value = { value }
          onChangeText = { onChangeText}
        />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: '#fffceb',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize:16,
    lineHeight: 23,
    flex: 3,
  },
  labelStyle: {
    fontSize: 16,
    paddingLeft: 20,
    flex: 1,
    color: '#fffceb',
  },
  containerStyle: {
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
};

export { AuthInput };
