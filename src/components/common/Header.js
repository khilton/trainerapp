//Import libraries
import React from  'react';
import { Text, View, Platform } from 'react-native';

//Make a component
const Header = (props) => {
  const { textStyle, viewStyle } = styles;

  return (
    <View style={viewStyle}>
      <Text style = {textStyle}>{props.headerText}</Text>
    </View>
  );
};

const styles = {

  viewStyle: {

    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    paddingTop: 15,
    backgroundColor: '#f8f8f8',
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 10, height: 1 },
        shadowOpacity: 0.9,
        shadowRadius: 3,
      },
      android: {
        elevation: 5,
      },
    }),
  },
  textStyle: {
    fontSize: 20
  },



};

//Make the component available
export {Header};
