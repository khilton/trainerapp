import React, { Component } from 'react';
import { View, Platform, DatePickerIOS } from 'react-native';


export const Body = Platform.select({
  ios: {
      render() {
      return (
        <View style={styles.container}>
          <DatePickerIOS
            date={this.state.chosenDate}
            onDateChange={this.setDate}
          />
        </View>
      );
    }
  },
  android: {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        date: new Date()
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        // Selected year, month (0-11), day
      }
    } catch ({code, message}) {
      console.warn('Cannot open date picker', message);
    }
  };
});
