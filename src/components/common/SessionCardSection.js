import React from 'react';
import { View } from 'react-native';

const SessionCardSection = (props) => {
  return (
    <View style = {[styles.containerStyle, props.style]}>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {
    borderBottomWidth: 1,
    padding: 0,
    backgroundColor: '#fff',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative',
    }
};

export  {SessionCardSection};
