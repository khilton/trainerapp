import React from 'react';
import { View, Platform } from 'react-native';

const AuthCard = (props) => {
  return(
    <View style={styles.constainerStyle}>
      {props.children}
    </View>
  );
};

const styles = {
  constainerStyle: {
    borderWidth: 0,
    borderRadius: 2,
    borderColor: '#fffceb',
    borderBottomWidth: 0,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 10,

  }
}

export { AuthCard };
