import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const HeaderButton = ({onPress, children}) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress ={onPress} style = {buttonStyle}>
      <Text style = {textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
  },
  buttonStyle: {
      alignSelf: 'stretch',
      marginLeft: 10,
      marginRight: 10,
  }
};

export { HeaderButton } ;
