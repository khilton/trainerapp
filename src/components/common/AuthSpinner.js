import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const AuthSpinner = ({ size }) => {
  return(
    <View style={styles.activityContainerStyle}>
      <ActivityIndicator
        color={'#fffceb'}
        size={ size || 'large' }
      />
    </View>
  );
};

const styles = {
  activityContainerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
};

export { AuthSpinner };
