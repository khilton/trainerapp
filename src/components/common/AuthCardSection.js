import React from 'react';
import { View } from 'react-native';

const AuthCardSection = (props) => {
  return (
    <View style = {[styles.containerStyle, props.style]}>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {
    borderBottomWidth: 0,
    padding: 5,
    backgroundColor: '#008901',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#fffceb',
    position: 'relative',
    }
};

export  { AuthCardSection };
