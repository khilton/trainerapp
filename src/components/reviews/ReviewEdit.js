import _ from 'lodash';
import React, { Component, Modal } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import ReviewForm from './ReviewForm';
import { reviewUpdate, reviewSave, reviewDelete } from '../../actions';
import {
  Card,
  CardSection,
  Button,
  Confirm
} from '../common';

class ReviewEdit extends Component {
  state= { showModal: false};

  componentWillMount() {
    _.each(this.props.navigation.state.params.review, (value, prop) => {
      this.props.reviewUpdate({ prop, value });
    });
  }

  onButtonPress() {
    const {
      sessionType,
      date,
      positive1,
      positive2,
      positive3,
      improvement1,
      improvement2,
      improvement3,
      enjoyable,
      effort,
      focus,
      navigation,
    } = this.props;

    this.props.reviewSave({
      sessionType: sessionType || "Practice",
      date,
      positive1,
      positive2,
      positive3,
      improvement1,
      improvement2,
      improvement3,
      enjoyable,
      effort: effort || "1",
      focus: focus || "1",
      navigation,
      uid: this.props.navigation.state.params.review.uid,
    });
  }

  onDeletePress() {
      this.setState({ showModal: !this.state.showModal});
  }

  onAccept() {
    this.props.reviewDelete({ uid: this.props.navigation.state.params.review.uid, navigation: this.props.navigation});
  }

  onDecline() {
    this.setState({ showModal: false});
  }

  render() {
      return(
        <ScrollView>
          <Card>
            <ReviewForm  {...this.props}/>
            <CardSection>
              <Button onPress={this.onButtonPress.bind(this)}>
                Save Changes
              </Button>
            </CardSection>

            <CardSection>
              <Button onPress={this.onDeletePress.bind(this)}>
                Delete Review
              </Button>
            </CardSection>
          </Card>
          <Confirm
            visible={this.state.showModal}
            onAccept={this.onAccept.bind(this)}
            onDecline={this.onDecline.bind(this)}
            >
            Are you sure you want to delete this
          </Confirm>
        </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    sessionType,
    date,
    positive1,
    positive2,
    positive3,
    improvement1,
    improvement2,
    improvement3,
    enjoyable,
    effort,
    focus,
  } = state.reviewForm;

  return {
    sessionType: sessionType,
    date: date,
    positive1: positive1,
    positive2: positive2,
    positive3: positive3,
    improvement1: improvement1,
    improvement2: improvement2,
    improvement3: improvement3,
    enjoyable: enjoyable,
    effort: effort,
    focus: focus,
  };
};

export default connect(mapStateToProps, {reviewUpdate, reviewSave, reviewDelete})(ReviewEdit);
