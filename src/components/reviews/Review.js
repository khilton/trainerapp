import React from 'react';
import { View, ScrollView, Text } from 'react-native';
import {
  Card,
  CardSection,
  Input,
  Button
} from '../common';

const Review = () => {
  return(
    <View>
      <Card>
        <CardSection>
          <Text>Session Name</Text>
          <Text>Date</Text>
        </CardSection>
        <CardSection>
          <Text>3 items you'll do well:</Text>
          <Text>...</Text>
        </CardSection>
        <CardSection>
          <Text>1 thing you'll enjoy:</Text>
          <Text>...</Text>
        </CardSection>
      </Card>
    </View>
  );
};

export default Review;
