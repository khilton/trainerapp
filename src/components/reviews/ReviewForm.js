import React, { Component } from 'react';
import { View, Text, Picker } from 'react-native';
import { connect } from 'react-redux';
import { reviewUpdate } from '../../actions';
import { CardSection, Input } from '../common';

class ReviewForm extends Component {
  render() {
    return(
      <View>
        <CardSection style={styles.containerStyle}>
          <Text style={styles.pickerLabelStyle}>Session Type</Text>
          <Picker
              selectedValue={this.props.sessionType}
              style={styles.pickerStyle}
              onValueChange={value => this.props.reviewUpdate({prop: 'sessionType', value})}>
              <Picker.Item label="Practice" value="Practice" />
              <Picker.Item label="Academy" value="Academy" />
              <Picker.Item label="Fitness" value="Fitness" />
              <Picker.Item label="Game" value="Game" />
              <Picker.Item label="Personal Session" value="Personal Session" />
            </Picker>
        </CardSection>

        <CardSection style={{flexDirection:'column'}}>
            <Text style={styles.pickerLabelStyle}>3 things you'll do well:</Text>

            <Input
              placeholder="..."
              label="1."
              value={this.props.positive1}
              onChangeText={text => this.props.reviewUpdate({prop: 'positive1', value: text})}
              />
            <Input
              placeholder="..."
              label="2."
              value={this.props.positive2}
              onChangeText={text => this.props.reviewUpdate({prop: 'positive2', value: text})}
              />
            <Input
              placeholder="..."
              label="3."
              value={this.props.positive3}
              onChangeText={text => this.props.reviewUpdate({prop: 'positive3', value: text})}
              />
        </CardSection>

        <CardSection style={{flexDirection:'column'}}>
            <Text style={styles.pickerLabelStyle}>3 things you'll work on:</Text>
            <Input
              placeholder="..."
              label="1."
              value={this.props.improvement1}
              onChangeText={text => this.props.reviewUpdate({prop: 'improvement1', value: text})}
              />
            <Input
              placeholder="..."
              label="2."
              value={this.props.improvement2}
              onChangeText={text => this.props.reviewUpdate({prop: 'improvement2', value: text})}
              />
            <Input
              placeholder="..."
              label="3."
              value={this.props.improvement3}
              onChangeText={text => this.props.reviewUpdate({prop: 'improvement3', value: text})}
              />
        </CardSection>

        <CardSection style={{flexDirection:'column'}}>
          <Text style={styles.pickerLabelStyle}>What is one thing you'll enjoy?</Text>
          <Input
            placeholder="..."
            label="1."
            value={this.props.enjoyable}
            onChangeText={text => this.props.reviewUpdate({prop: 'enjoyable', value: text})}
            />
        </CardSection>

        <CardSection>
          <Text style={styles.pickerLabelStyle}>Effort</Text>
          <Picker
            selectedValue={this.props.effort}
            style={styles.pickerStyle}
            onValueChange={value => this.props.reviewUpdate({prop: 'effort', value})}>
            <Picker.Item label="1" value="1" />
            <Picker.Item label="2" value="2" />
            <Picker.Item label="3" value="3" />
            <Picker.Item label="4" value="4" />
            <Picker.Item label="5" value="5" />
            <Picker.Item label="6" value="6" />
            <Picker.Item label="7" value="7" />
            <Picker.Item label="8" value="8" />
            <Picker.Item label="9" value="9" />
            <Picker.Item label="10" value="10" />
          </Picker>
        </CardSection>

        <CardSection>
          <Text style={styles.pickerLabelStyle}>Focus</Text>
          <Picker
            selectedValue={this.props.focus}
            style={styles.pickerStyle}
            onValueChange={value => this.props.reviewUpdate({prop: 'focus', value})}>
            <Picker.Item label="1" value="1" />
            <Picker.Item label="2" value="2" />
            <Picker.Item label="3" value="3" />
            <Picker.Item label="4" value="4" />
            <Picker.Item label="5" value="5" />
            <Picker.Item label="6" value="6" />
            <Picker.Item label="7" value="7" />
            <Picker.Item label="8" value="8" />
            <Picker.Item label="9" value="9" />
            <Picker.Item label="10" value="10" />
          </Picker>
        </CardSection>
      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
  },
  pickerLabelStyle: {
    fontSize: 18,
    paddingLeft:20,
  },
  pickerStyle: {
    flex: 1,
  },
}

const mapStateToProps = (state) => {
  const {
    sessionType,
    date,
    positive1,
    positive2,
    positive3,
    improvement1,
    improvement2,
    improvement3,
    enjoyable,
    effort,
    focus,
  } = state.reviewForm;

  return{
    sessionType: sessionType,
    date: date,
    positive1: positive1,
    positive2: positive2,
    positive3: positive3,
    improvement1: improvement1,
    improvement2: improvement2,
    improvement3: improvement3,
    enjoyable: enjoyable,
    effort: effort,
    focus: focus,
  };
};

export default connect(mapStateToProps, {reviewUpdate})(ReviewForm);
