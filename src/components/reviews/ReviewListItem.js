import React, { Component } from 'react';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
import { CardSection } from '../common';
import { withNavigation } from 'react-navigation';

class ReviewListItem extends Component {

  onRowPress() {
      this.props.navigation.navigate('reviewEdit', { review: this.props.review });
  }

  render() {
    const { date } = this.props.review;

    return (
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View>
          <CardSection>
            <Text style = {styles.dateStyle}>
              {date}
            </Text>
          </CardSection>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = {
  dateStyle: {
    fontSize: 18,
    paddingLeft: 15,
  }
};

export default withNavigation(ReviewListItem);
