import _ from 'lodash';
import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { reviewUpdate, reviewCreate } from '../../actions';
import ReviewForm from './ReviewForm';
import {
  Button,
  Card,
  CardSection,
  Spinner,
} from '../common';

class ReviewCreate extends Component {

  onButtonPress(){
    const {
      sessionType,
      date,
      positive1,
      positive2,
      positive3,
      improvement1,
      improvement2,
      improvement3,
      enjoyable,
      effort,
      focus,
      navigation
    } = this.props;

    this.props.reviewCreate({
      sessionType: sessionType || "Practice",
      date,
      positive1,
      positive2,
      positive3,
      improvement1,
      improvement2,
      improvement3,
      enjoyable,
      effort: effort || "1",
      focus: focus || "1",
      navigation,
    });
  }

  render() {
    return(
      <ScrollView>
        <Card>
          <ReviewForm {...this.props} />
          <CardSection>
            <Button onPress={this.onButtonPress.bind(this)}>
              Create
            </Button>
          </CardSection>
        </Card>
      </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    sessionType,
    date,
    positive1,
    positive2,
    positive3,
    improvement1,
    improvement2,
    improvement3,
    enjoyable,
    effort,
    focus,
  } = state.reviewForm;

  return{
    sessionType: sessionType,
    date: date,
    positive1: positive1,
    positive2: positive2,
    positive3: positive3,
    improvement1: improvement1,
    improvement2: improvement2,
    improvement3: improvement3,
    enjoyable: enjoyable,
    effort: effort,
    focus: focus,
  };
};


export default connect(mapStateToProps, {reviewUpdate, reviewCreate})(ReviewCreate);
