import _ from 'lodash';
import React, { Component } from 'react';
import { ListView, View, Image, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import Review from './Review';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import ReviewListItem from './ReviewListItem';
import {
  Button,
  HeaderButton
} from '../common';
import { reviewsFetch } from '../../actions';

class ReviewsScreen extends Component {

  componentWillMount(){
    this.props.reviewsFetch();
    this.createDataSource(this.props);
  }

  componentWillReceiveProps(nextProps) {
    // nextProps are the next set of nextProps
    //this.props is still the old set
    this.createDataSource(nextProps);
  }

  createDataSource({ reviews }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    console.log(reviews);
    this.dataSource = ds.cloneWithRows(reviews);
  }

  renderRow(review){
      return (
        <ReviewListItem
          review={review}
        />
      );
  }

  render() {
    return(
      <View style={styles.mainContainerStyle}>
        <Image
          source={require('../resources/Images/TrainMindfully.png')}
          style={{width: Dimensions.get('window').width, height: 160, alignSelf: 'flex-end'}}
        />
        <ListView
          enableEmptySections
          dataSource={this.dataSource}
          renderRow={this.renderRow}
          style={{paddingTop:5}}
        />
      </View>
    );
  }
}

const styles = {
  containerStyle: {
    padding: 5,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
  },
  mainContainerStyle: {
    flex: 1,
    justifyContent: 'right',
  },
}

const mapStateToProps = (state) => {
  const reviews = _.map(state.reviews, (val, uid) => {
      return {...val, uid};
  });

  return { reviews };
};

export default connect(mapStateToProps, { reviewsFetch })(ReviewsScreen);
