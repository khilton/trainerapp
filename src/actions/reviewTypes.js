export const REVIEW_UPDATE = 'review_update';
export const REVIEW_CREATE = 'review_create';
export const REVIEWS_FETCH_SUCCESS = 'reviews_fetch_success';
export const REVIEWS_SAVE_SUCCESS = 'reviews_save_success';
