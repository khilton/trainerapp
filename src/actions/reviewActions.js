import firebase from '@firebase/app';
import '@firebase/auth';
import '@firebase/database';
import {
  REVIEW_UPDATE,
  REVIEW_CREATE,
  REVIEWS_FETCH_SUCCESS,
  REVIEWS_SAVE_SUCCESS,
} from './reviewTypes';

export const reviewUpdate = ({ prop, value }) => {
  return {
    type:  REVIEW_UPDATE,
    payload: { prop, value}
  };
};

export const reviewCreate = ({
  sessionType,
  date,
  positive1,
  positive2,
  positive3,
  improvement1,
  improvement2,
  improvement3,
  enjoyable,
  effort,
  focus,
  navigation,
}) => {
    const { currentUser} = firebase.auth();

    return (dispatch) => {
      firebase.database().ref(`users/${currentUser.uid}/reviews`)
      .push({
        sessionType,
        date,
        positive1,
        positive2,
        positive3,
        improvement1,
        improvement2,
        improvement3,
        enjoyable,
        effort,
        focus,
      })
      .then(() => {
        dispatch({ type: REVIEW_CREATE})
        navigation.navigate('reviewList')
        } )
    };

};

export const reviewsFetch = () => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
      firebase.database().ref(`users/${currentUser.uid}/reviews`)
        .on('value', snapshot => {
          dispatch({ type: REVIEWS_FETCH_SUCCESS, payload: snapshot.val() });
        })
    };
};

export const reviewSave = ({
  sessionType,
  date,
  positive1,
  positive2,
  positive3,
  improvement1,
  improvement2,
  improvement3,
  enjoyable,
  effort,
  focus,
  navigation,
  uid,
}) => {

  const { currentUser } = firebase.auth();

return(dispatch) => {
  firebase.database().ref(`/users/${currentUser.uid}/reviews/${uid}`)
    .set({
      sessionType,
      date,
      positive1,
      positive2,
      positive3,
      improvement1,
      improvement2,
      improvement3,
      enjoyable,
      effort,
      focus,
    })
    .then(() =>{
      dispatch({ type: REVIEWS_SAVE_SUCCESS });
      navigation.navigate('reviewList');
    });
  };
};

export const reviewDelete = ({ uid, navigation }) => {
  const { currentUser } = firebase.auth();

  return() => {
    firebase.database().ref(`/users/${currentUser.uid}/reviews/${uid}`)
      .remove()
      .then(() => {
        navigation.navigate('reviewList');
      });
  };
};
