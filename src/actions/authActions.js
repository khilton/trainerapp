import axios from 'axios';
import firebase from 'firebase';
import {
  PHONE_CHANGED,
  CODE_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  REGISTER_USER_FAIL,
  REGISTER_USER,
  PROFILE_UPDATE
} from './authTypes';

const ROOT_URL = 'https://us-central1-dev-player-77051.cloudfunctions.net';

export const profileUpdate = ({ prop, value }) => {
  return {
    type:  PROFILE_UPDATE,
    payload: { prop, value}
  };
};

export const phoneChanged = (text) => {
  return{
    type: PHONE_CHANGED,
    payload: text
  };
};

export const codeChanged = (text) => {
  return{
    type: CODE_CHANGED,
    payload: text
  };
};

export const loginUser = async ({ phone, code, error, navigation }) => {
  return(dispatch) => {
    dispatch({ type: LOGIN_USER});
    try {
      let { data } = await axios.post(`${ROOT_URL}/verifyOneTimePassword`, {phone: phone, code: code});
      firebase.auth().signInWithCustomToken(data.token);
    }
    catch (dispatch) {
      this.loginUserFail(dispatch);
    };
  };
};

export const registerUser = async ({ phone, error, navigation }) => {
  return(dispatch) => {
    dispatch({ type: REGISTER_USER});
    try {
      await axios.post(`${ROOT_URL}/createUser`, {phone: phone})
      await axios.post(`${ROOT_URL}/requestOneTimePassword`, {phone: phone})
    }
    catch (dispatch) {
      this.registerUserFail(dispatch);
    };
  };
};

const loginUserFail = (dispatch) => {
  dispatch({ type: LOGIN_USER_FAIL })
};

const loginUserSuccess = (dispatch, user, navigation) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });

  navigation.navigate('Main');
};

const registerUserFail = (dispatch) => {
  dispatch({ type: REGISTER_USER_FAIL })
};
