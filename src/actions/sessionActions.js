import {
  CHANGE_DRILL_INDEX,
  SET_CURRENT_DRILL,
  GET_DRILLS,
  GET_SESSIONS,
  GET_DRILLS_SUCCESS,
  GET_DRILLS_FAIL,
  LOAD_SESSION,
  LOAD_SESSION_SUCCESS,
  LOAD_SESSION_FAILURE,
  PLAY_SESSION,
  PAUSE_SESSION
} from '../actions/sessionTypes';
import firebase from '@firebase/app';
import '@firebase/auth';
import '@firebase/database';

export const changeDrillIndex = ( drillIndex ) => {
  return(dispatch) => {
    dispatch({
      type: CHANGE_DRILL_INDEX,
      payload: drillIndex
    });
  };
};

export const setCurrentDrill = ( drillIndex, drills ) => {
  return(dispatch) => {
    dispatch({
      type: SET_CURRENT_DRILL,
      payload: drills[drillIndex]
    });
  };
};

export const getSessions = () => {
    return (dispatch) => {
      firebase.database().ref(`/sessions`)
        .on('value', snapshot => {
          dispatch({
            type: GET_SESSIONS,
            payload: Object.values(snapshot.val())
          });
        })
    };
};

export const loadSession = (drills) => {
    return (dispatch) => {
        dispatch({
          type: LOAD_SESSION
        });
        getDrills(drills, dispatch)
          .then((fullDrillList) => {
            dispatch({
              type: LOAD_SESSION_SUCCESS
            });
          });
    };
};

export function getDrills(drills, dispatch) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      dispatch({ type: GET_DRILLS });
      return fullDrillList = getDrillArray(drills)
      .then((fullDrillList) => {
        dispatch({
          type: GET_DRILLS_SUCCESS,
          payload: fullDrillList
        });
        dispatch({
          type: SET_CURRENT_DRILL,
          payload: fullDrillList[0]
        });
        resolve();
      });
    }, 1000);
  });
};

function getDrillArray(drills, dispatch){
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      var fullDrillList = [];
      const drillArray = Object.values(drills);
      drillArray.forEach((drill) => {
        getDrill(drill)
        .then((fullDrill) => {
          fullDrillList.push(fullDrill);
          if (drillArray.length === fullDrillList.length ) {
            fullDrillList.sort(dynamicSort("sessionIndex"));
            resolve(fullDrillList);
          }
        });
      });

    }, 200);
  });
};

function getDrill(drill) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      var error = false;
      firebase.database().ref(`/exercises/${drill.exercise}`)
        .on('value', snapshot => {
          const fullDrill = Object.assign(snapshot.val(), drill);
          if (!error) {
            resolve(fullDrill);
          } else {
            reject("Drill wouldn't load");
          }
        });

    }, 200);
  });
};

function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
};

export const playSession = () => {
  return(dispatch) => {
    dispatch({
      type: PLAY_SESSION
    });
  };
};

export const pauseSession = () => {
  return(dispatch) => {
    dispatch({
      type: PAUSE_SESSION
    });
  };
};
