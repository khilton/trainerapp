import React from 'react';
import { Scene, Router, createStackNavigator } from 'react-navigation';
import Button from '../src/components/common/Button';
import LoginForm from '../src/components/authorization/LoginForm';
import SignUpForm from '../src/components/authorization/SignUpForm';
import ProfileForm from '../src/components/authorization/ProfileForm';

const AuthRouterComponent = createStackNavigator({
    login: {
      screen: LoginForm,
      navigationOptions: () => ({
         title: 'Login',
       })
     },
     signUp: {
       screen: SignUpForm,
       navigationOptions: () => ({
          title: 'Sign Up',
        })
      },
      editProfile: {
        screen: ProfileForm,
        navigationOptions: () => ({
           title: 'Profile',
         })
       },
  },

  { initialRouteName: 'login'}

);

export default AuthRouterComponent;
