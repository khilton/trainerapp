import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import SessionScreen from '../src/components/videos/SessionScreen';
import SessionSelectionScreen from '../src/components/videos/SessionSelectionScreen';
import ReviewsScreen from '../src/components/reviews/ReviewsScreen';
import ReviewCreate from '../src/components/reviews/ReviewCreate';
import ReviewEdit from '../src/components/reviews/ReviewEdit';
import SettingsScreen from '../screens/SettingsScreen';
import { HeaderButton } from '../src/components/common';


const SessionStack = createStackNavigator({
  sessionHome: {
    screen: SessionSelectionScreen,
    navigationOptions: () => ({
       title: 'Sessions',
     })
   },
   sessionPage: {
     screen: SessionScreen,
     navigationOptions: (session) => ({
        title: 'Session',
      })
    },
});

SessionStack.navigationOptions = {
  tabBarLabel: 'Sessions',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

/*
const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
};
*/

const ReviewsStack = createStackNavigator({
    reviewList: {
      screen: ReviewsScreen,
      navigationOptions: ( {navigation} ) => {
        return {
          title: 'Reviews',
          headerRight: (
           <HeaderButton
            onPress={() => {navigation.navigate({routeName: 'reviewCreate'})}}
            title="Info"
            color="#fff"
           >
             New
           </HeaderButton>
         ),
       }
     }
    },
    reviewCreate: {
      screen: ReviewCreate,
      navigationOptions: () => ({
         title: 'Review',
       })
    },
    reviewEdit: {
      screen: ReviewEdit,
      navigationOptions: () => ({
         title: 'Edit Review',
       })
    },
  },

  { initialRouteName: 'reviewList'}

);

ReviewsStack.navigationOptions = {
  tabBarLabel: 'Reviews',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-link' : 'md-link'}
    />
  ),
};

export default createBottomTabNavigator({
  SessionStack,
  ReviewsStack,
});
